---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Elios
nom: Engammare

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profil
![Elios](media/Elios-Engammare.jpg)  
Bonjour, je suis Elios Engammare.  
Âgé de 19 ans et actuellement en BTS SIO première année, Je suis un étudiant passionné par l'informatique que ce soit en programmation ou en réseau et je compte bien faire carrière dans ce secteur.  

Actuellement en recherche de stage pour une durée de 6 semaine du 13 Mai au 28 Juin 2024.

## Contact
Mon compte [Linkedin](https://www.linkedin.com/in/elios-engammare/)  
Adresse mail pour me contacter : [Elios ENGAMMARE](mailto:elios@engamma.re)  

# Formations
* Septembre 2022  
**BTS SIO 1ère année**[(Services informatiques au organisations)](https://www.letudiant.fr/etudes/bts/bts-sio-services-informatiques-aux-organisations.html), sur _Nantes à la Colinière_ 
* 2019 - 2022  
**Lycée général et technologique STI2D** [(option SIN)](https://etudiant.lefigaro.fr/vos-etudes/etudes-secondaires/33902-bac-sti2d-option-sin-tout-savoir/),sur _Blain à Camille Claudel_


# Compétences

| Compétences  | Niveau ou type      |
| :--------------- |:----------------|
| Maitrise de l'Anglais | Niveau B1  |
| Base de Données | SGBD (PostreSQL) / SQL |
| POO |Python / java|
| Développement Web  |  HTML / CSS / PHP  |
|  OS  |  Windows / Linux  |
|  Outils utilisés  |  Git / Framagit / PhpStorm / PyCharm  |

# Expérience professionnelles
## Stage GSG
* Stage de 5 semaine dans l'entreprise GSG (partenaire de la plateforme Iggral)  
Durant mon stage dans l’entreprise GSG (Global Saving Group), j’ai pu en apprendre beaucoup sur la vie en entreprise, le travail d’équipe mais aussi l’assiduité et le comportement au travail. Le travail en entreprise est très différent du travail en école, les obligations et les attentes sont plus grandes et plus restrictives.

## Expériences qui enrichissent mon choix de spécialité [(SISR)](https://www.orientation.com/diplomes/bts-services-informatiques-aux-organisations-option-a-solutions-dinfrastructure-systemes-et-reseaux)

Passionner d'informatique depuis mon enfance, j'ai toujours été émerveiller devant toutes les techniques qui nous entours.

* Au collège déjà j'ai pu faire un stage d'observation dans l'entreprise GFI spécialisé dans le numérique, ils s'occupaient d'une infrastructure réseau d'un ils s'occupaient d'une infrastructure réseau d'un hôpital public.

* Suite à cela j'ai pus avoir mon stage dans l'entreprise GSG, j'ai pu y apprendre beacoup de choses sur l'environement de travail et aussi sur le réseau, même si j'ai pris la spécialité SLAM [(Solutions Logiciels aux Applications Métiers)](https://www.orientation.com/diplomes/bts-services-informatiques-aux-organisations-option-b-solutions-logicielles-et-applications-metiers) durant ma première année de BTS.
